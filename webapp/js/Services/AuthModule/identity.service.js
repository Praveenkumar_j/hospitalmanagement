"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var core_2 = require("angular2-cookie/core");
var IdentityService = /** @class */ (function () {
    function IdentityService(_cookieService) {
        this.cookieService = _cookieService;
        this.refreshCookie();
    }
    IdentityService.prototype.refreshCookie = function () {
        this.isAuthenticated = false;
        var profileCookie = this.cookieService.get("sessionId");
        if (profileCookie != undefined && typeof profileCookie == "string") {
            profileCookie = atob(profileCookie);
            var profileCookieArr = profileCookie.split("??");
            if (profileCookieArr.length == 3) {
                this.user = atob(profileCookieArr[0]);
                this.role = atob(profileCookieArr[2]);
                this.isAuthenticated = true;
            }
        }
    };
    IdentityService.prototype.setAuthCookie = function (authObj) {
        this.isAuthenticated = true;
        var profileCookie = btoa(btoa(authObj.name) + "??" + btoa(authObj.password) + "??" + btoa(authObj.role));
        this.cookieService.put("sessionId", profileCookie);
        this.refreshCookie();
    };
    IdentityService.prototype.clearIdentity = function () {
        this.cookieService.remove("sessionId");
        this.isAuthenticated = false;
        this.refreshCookie();
    };
    IdentityService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [core_2.CookieService])
    ], IdentityService);
    return IdentityService;
}());
exports.IdentityService = IdentityService;
//# sourceMappingURL=identity.service.js.map