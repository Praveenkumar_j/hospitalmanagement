"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("rxjs/add/operator/map");
require("rxjs/add/operator/filter");
var identity_service_1 = require("./identity.service");
var AuthService = /** @class */ (function () {
    function AuthService(_identity, _http) {
        this.identity = _identity;
        this.http = _http;
    }
    AuthService.prototype.authenticate = function (username, password) {
        var _this = this;
        return this.http.get("./mocks/logins.json").map(function (response) {
            var user = response.json().users.filter(function (user) { return user.name == username && user.password == password; });
            console.log(user);
            if (user.length > 0) {
                _this.identity.setAuthCookie(user[0]);
                return true;
            }
            else {
                _this.identity.clearIdentity();
                return false;
            }
        });
    };
    AuthService.prototype.kickoff = function () {
        this.identity.clearIdentity();
    };
    AuthService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [identity_service_1.IdentityService, http_1.Http])
    ], AuthService);
    return AuthService;
}());
exports.AuthService = AuthService;
//# sourceMappingURL=auth.service.js.map