"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var auth_service_1 = require("../../Services/AuthModule/auth.service");
var loginevent_model_1 = require("./../../Models/loginevent.model");
var loginevent_service_1 = require("./../../Services/AuthModule/loginevent.service");
var LoginComponent = /** @class */ (function () {
    /*@Output()
    private onLoginSuccess:EventEmitter<void> = new EventEmitter<void>();
 
    @Output()
    private onLoginFailure:EventEmitter<void> = new EventEmitter<void>();*/
    function LoginComponent(_auth, _logineventservice) {
        this.username = "";
        this.password = "";
        this.auth = _auth;
        this.logineventservice = _logineventservice;
    }
    LoginComponent.prototype.login = function (event) {
        var _this = this;
        event.preventDefault();
        var eventmodel = new loginevent_model_1.LoginEventModel();
        this.auth.authenticate(this.username, this.password).subscribe(function (response) {
            if (response == true) {
                eventmodel.loginsuccess = true;
                eventmodel.message = "success";
                //this.onLoginSuccess.emit();
                _this.logineventservice.customEmit(eventmodel);
            }
            else {
                eventmodel.loginsuccess = true;
                eventmodel.message = "failure";
                //this.onLoginFailure.emit();
                _this.logineventservice.customEmit(eventmodel);
            }
        }, function (error) {
            eventmodel.loginsuccess = false;
            eventmodel.message = "failure";
            //this.onLoginFailure.emit();
            _this.logineventservice.customEmit(eventmodel);
        });
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], LoginComponent.prototype, "username", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], LoginComponent.prototype, "password", void 0);
    LoginComponent = __decorate([
        core_1.Component({
            selector: "login",
            templateUrl: "/webapp/templates/login.component.html"
        }),
        __metadata("design:paramtypes", [auth_service_1.AuthService, loginevent_service_1.LoginEventService])
    ], LoginComponent);
    return LoginComponent;
}());
exports.loginRoute = { name: 'login', url: '/login', component: LoginComponent };
var LoginModule = /** @class */ (function () {
    function LoginModule() {
    }
    LoginModule = __decorate([
        core_1.NgModule({
            declarations: [LoginComponent],
            //exports:[LoginComponent],
            providers: [auth_service_1.AuthService]
        })
    ], LoginModule);
    return LoginModule;
}());
exports.LoginModule = LoginModule;
//# sourceMappingURL=login.module.js.map