"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var hospital_routes_1 = require("./hospital.routes");
var login_module_1 = require("./LoginModule/login.module");
var admin_module_1 = require("./AdminModule/admin.module");
var platform_browser_1 = require("@angular/platform-browser");
var auth_module_1 = require("../Services/AuthModule/auth.module");
var identity_service_1 = require("../Services/AuthModule/identity.service");
var loginevent_service_1 = require("../Services/AuthModule/loginevent.service");
var angular_1 = require("@uirouter/angular");
//declare var $:JQueryStatic;
var HospitalAppComponent = /** @class */ (function () {
    function HospitalAppComponent(_identity, _router, _logineventservice) {
        this.identity = _identity;
        this.router = _router;
        this.logineventservice = _logineventservice;
    }
    HospitalAppComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.changeRoute();
        this.logineventservice.loginevent.subscribe(function (data) {
            _this.changeRoute();
        });
    };
    /*loginSuccess():void
    {
      this.changeRoute();
    }
    loginFailure():void
    {
      alert("Login failed");
    }*/
    HospitalAppComponent.prototype.changeRoute = function () {
        if (this.identity.isAuthenticated) {
            var user_role = this.identity.role;
            if (user_role != undefined && typeof user_role == 'string') {
                console.log(user_role);
                switch (parseInt(user_role)) {
                    case 0:
                        this.router.stateService.go("admin");
                        break;
                    case 1:
                        break;
                    case 2:
                        break;
                    case 3:
                        break;
                }
            }
        }
        else {
            this.router.stateService.go("login");
        }
    };
    HospitalAppComponent = __decorate([
        core_1.Component({
            selector: "hospital",
            templateUrl: "/webapp/templates/account.component.html"
        }),
        __metadata("design:paramtypes", [identity_service_1.IdentityService, angular_1.UIRouter, loginevent_service_1.LoginEventService])
    ], HospitalAppComponent);
    return HospitalAppComponent;
}());
var HospitalModule = /** @class */ (function () {
    function HospitalModule() {
    }
    HospitalModule = __decorate([
        core_1.NgModule({
            imports: [hospital_routes_1.RoutingConfigModule, platform_browser_1.BrowserModule, login_module_1.LoginModule, auth_module_1.AuthModule, admin_module_1.AdminModule],
            declarations: [HospitalAppComponent],
            bootstrap: [HospitalAppComponent],
            providers: [identity_service_1.IdentityService]
        })
    ], HospitalModule);
    return HospitalModule;
}());
exports.HospitalModule = HospitalModule;
//# sourceMappingURL=hospital.module.js.map