"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var login_module_1 = require("./LoginModule/login.module");
var admin_module_1 = require("./AdminModule/admin.module");
var angular_1 = require("@uirouter/angular");
function UIRouterStateConfig(router, injector) {
    //router.urlService.rules.otherwise({state:"login"});
}
exports.RoutingConfigModule = angular_1.UIRouterModule.forRoot({
    useHash: true,
    states: [login_module_1.loginRoute, admin_module_1.adminRoute],
    config: UIRouterStateConfig
});
//# sourceMappingURL=hospital.routes.js.map