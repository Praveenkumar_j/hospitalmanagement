"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var auth_service_1 = require("./../../Services/AuthModule/auth.service");
var loginevent_service_1 = require("./../../Services/AuthModule/loginevent.service");
var loginevent_model_1 = require("./../../Models/loginevent.model");
var AdminComponent = /** @class */ (function () {
    function AdminComponent(_authservice, _logineventservice) {
        this._authservice = _authservice;
        this._logineventservice = _logineventservice;
    }
    AdminComponent.prototype.Logout = function () {
        this._authservice.kickoff();
        var eventmodel = new loginevent_model_1.LoginEventModel();
        eventmodel.loginsuccess = false;
        eventmodel.message = "logout";
        this._logineventservice.customEmit(eventmodel);
    };
    AdminComponent = __decorate([
        core_1.Component({
            selector: "admin",
            templateUrl: "/webapp/templates/admin.component.html"
        }),
        __metadata("design:paramtypes", [auth_service_1.AuthService, loginevent_service_1.LoginEventService])
    ], AdminComponent);
    return AdminComponent;
}());
exports.adminRoute = { name: 'admin', url: '/admin', component: AdminComponent };
var AdminModule = /** @class */ (function () {
    function AdminModule() {
    }
    AdminModule = __decorate([
        core_1.NgModule({
            declarations: [AdminComponent],
            providers: [auth_service_1.AuthService]
        })
    ], AdminModule);
    return AdminModule;
}());
exports.AdminModule = AdminModule;
//# sourceMappingURL=admin.module.js.map