import {ModuleWithProviders} from "@angular/core"
import {loginRoute} from "./LoginModule/login.module";
import {adminRoute} from "./AdminModule/admin.module";
import {Routes,RouterModule} from "@angular/router"
import {UIRouterModule, UIRouter, UIInjector} from "@uirouter/angular";

function UIRouterStateConfig(router:UIRouter, injector: UIInjector)
{
  //router.urlService.rules.otherwise({state:"login"});
}


export const RoutingConfigModule:ModuleWithProviders = UIRouterModule.forRoot({
    useHash:true,
    states: [loginRoute,adminRoute],
    config:UIRouterStateConfig
});


