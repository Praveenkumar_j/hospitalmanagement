import {NgModule,Component} from "@angular/core";
import {AuthService} from "./../../Services/AuthModule/auth.service";
import {LoginEventService} from "./../../Services/AuthModule/loginevent.service";
import {LoginEventModel} from "./../../Models/loginevent.model";

@Component({
    selector:"admin",
    templateUrl:"/webapp/templates/admin.component.html"
})
class AdminComponent
{
  constructor(private _authservice:AuthService,private _logineventservice:LoginEventService)
  {

  }
  Logout():void
  {
      this._authservice.kickoff();
      let eventmodel:LoginEventModel = new LoginEventModel();
      eventmodel.loginsuccess=false;
      eventmodel.message="logout";
      this._logineventservice.customEmit(eventmodel);
  }
}
export const adminRoute = {name:'admin',url:'/admin',component:AdminComponent};
@NgModule({
    declarations:[AdminComponent],
    providers:[AuthService]
})
export class AdminModule
{

}