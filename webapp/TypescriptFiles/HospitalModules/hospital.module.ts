import {NgModule,Component,OnInit} from "@angular/core";
import {RoutingConfigModule} from "./hospital.routes";
import {LoginModule} from "./LoginModule/login.module";
import {AdminModule} from "./AdminModule/admin.module";
import {BrowserModule} from "@angular/platform-browser";
import {AuthModule} from "../Services/AuthModule/auth.module";
import {IdentityService} from "../Services/AuthModule/identity.service";
import {LoginEventService} from "../Services/AuthModule/loginevent.service";
import {LoginEventModel}  from "../Models/loginevent.model";
import {UIRouter} from "@uirouter/angular";

import * as bootstrap from "bootstrap";
import * as $ from "jquery";
//declare var $:JQueryStatic;

@Component({
    selector:"hospital",
    templateUrl:"/webapp/templates/account.component.html"
})
class HospitalAppComponent implements OnInit
{
    private identity:IdentityService;
    private router:UIRouter;
    private logineventservice:LoginEventService;
    constructor(_identity:IdentityService,_router:UIRouter,_logineventservice:LoginEventService)
    {
        this.identity = _identity;
        this.router = _router;
        this.logineventservice=_logineventservice;
    }
    ngOnInit():void
    {
      this.changeRoute();
      this.logineventservice.loginevent.subscribe( (data:LoginEventModel)=>{
          this.changeRoute();
      })
    }
    /*loginSuccess():void
    {
      this.changeRoute();
    }
    loginFailure():void
    {
      alert("Login failed");
    }*/
    private changeRoute():void
    {
        if(this.identity.isAuthenticated)
        {
            let user_role = this.identity.role;
            if(user_role != undefined && typeof user_role=='string')
            {
                console.log(user_role);
                switch(parseInt(user_role))
                {
                    case 0:
                    this.router.stateService.go("admin");
                    break;
                    case 1:
                    break;
                    case 2:
                    break;
                    case 3:
                    break;
                }
            }
        }
        else
        {
            this.router.stateService.go("login");
        }
    }
}

@NgModule({
    imports:[RoutingConfigModule,BrowserModule,LoginModule,AuthModule,AdminModule],
    declarations:[HospitalAppComponent],
    bootstrap:[HospitalAppComponent],
    providers:[IdentityService]
})
export class HospitalModule
{
 
}
