import {NgModule,Component,Input,Output,EventEmitter} from "@angular/core";
import {AuthService} from "../../Services/AuthModule/auth.service";
import {LoginEventModel} from "./../../Models/loginevent.model"
import {LoginEventService} from "./../../Services/AuthModule/loginevent.service";


@Component({
    selector:"login",
    templateUrl:"/webapp/templates/login.component.html"
})
class LoginComponent
{
   private auth:AuthService;
   private logineventservice:LoginEventService;
   
   @Input()
   private username:string="";
   @Input()
   private password:string="";

   /*@Output()
   private onLoginSuccess:EventEmitter<void> = new EventEmitter<void>();

   @Output()
   private onLoginFailure:EventEmitter<void> = new EventEmitter<void>();*/

   constructor(_auth:AuthService,_logineventservice:LoginEventService)
   {
    this.auth = _auth;
    this.logineventservice = _logineventservice;
   }

   login(event:_Event):void
   {
     event.preventDefault();
     let eventmodel = new LoginEventModel();
     this.auth.authenticate(this.username,this.password).subscribe(response=>
     {
       if(response==true)
       {  
          eventmodel.loginsuccess=true;
          eventmodel.message="success";
          //this.onLoginSuccess.emit();
          this.logineventservice.customEmit(eventmodel);
       }
       else
       {
          eventmodel.loginsuccess=true;
          eventmodel.message="failure";
          //this.onLoginFailure.emit();
          this.logineventservice.customEmit(eventmodel);
       }
     },error=>{
        eventmodel.loginsuccess=false;
        eventmodel.message="failure";
        //this.onLoginFailure.emit();
        this.logineventservice.customEmit(eventmodel);
     });
     
   }
}

export const loginRoute = {name:'login',url:'/login',component:LoginComponent}

@NgModule({
 declarations:[LoginComponent],
 //exports:[LoginComponent],
 providers:[AuthService]
})
export class LoginModule
{

}