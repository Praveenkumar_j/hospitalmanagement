import {platformBrowserDynamic} from "@angular/platform-browser-dynamic"
import {HospitalModule} from "./Hospitalmodules/hospital.module"
platformBrowserDynamic().bootstrapModule(HospitalModule);