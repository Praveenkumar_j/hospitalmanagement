import {Injectable} from "@angular/core";
import {Http,Response} from "@angular/http";
import "rxjs/add/operator/map";
import "rxjs/add/operator/filter";
import {Observable} from "rxjs/Observable";
import {Observer} from "rxjs/Observer";
import {IdentityService} from "./identity.service";

@Injectable()
export class AuthService
{
  private identity:IdentityService;
  private http:Http;  
  constructor(_identity:IdentityService,_http:Http)
  {
    this.identity = _identity;
    this.http = _http;
  }

  authenticate(username:string, password:string):Observable<boolean>
  {
    return this.http.get("./mocks/logins.json").map((response:Response)=>
    {
       let user:Array<any> = response.json().users.filter((user:any)=>user.name==username && user.password==password);
       console.log(user);
       if(user.length>0)
       {
         this.identity.setAuthCookie(user[0]);
         return true;
       }
       else
       {
        this.identity.clearIdentity();
        return false;
       }
    });
  }
  kickoff():void
  {
    this.identity.clearIdentity();
  }
}