import {NgModule} from "@angular/core";
import {Injectable} from "@angular/core";
import {IdentityService} from "./identity.service";
import {AuthService} from "./auth.service";
import {LoginEventService} from "./loginevent.service"
import {HttpModule} from "@angular/http";
import {CookieService} from "angular2-cookie/services/cookies.service";

@NgModule({
    imports:[HttpModule],
    providers:[AuthService,IdentityService,CookieService,LoginEventService]
})
export class AuthModule
{

}