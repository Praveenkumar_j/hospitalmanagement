import {Injectable} from "@angular/core";
import {CookieService} from "angular2-cookie/core";

@Injectable()
export class IdentityService
{
  isAuthenticated:boolean;
  user:string;
  role:string;
  private cookieService:CookieService
  constructor(_cookieService:CookieService)
  {
    this.cookieService = _cookieService;
    this.refreshCookie();
  }

  private refreshCookie():void
  {
    this.isAuthenticated=false;
    let profileCookie:string = this.cookieService.get("sessionId");
    if(profileCookie!=undefined && typeof profileCookie == "string")
    {
      profileCookie = atob(profileCookie);
      let profileCookieArr:Array<string> = profileCookie.split("??");
      if(profileCookieArr.length==3)
      {
        this.user = atob(profileCookieArr[0]);
        this.role = atob(profileCookieArr[2]);
        this.isAuthenticated=true;
      }
    }
  }

  setAuthCookie(authObj:any):void
  {
      this.isAuthenticated=true;
      let profileCookie:string = btoa(btoa(authObj.name)+"??"+btoa(authObj.password)+"??"+btoa(authObj.role));
      this.cookieService.put("sessionId",profileCookie);
      this.refreshCookie();
  }

  clearIdentity():void
  {
    this.cookieService.remove("sessionId");
    this.isAuthenticated=false;
    this.refreshCookie();
  }

}