import {EventEmitter,Injectable} from "@angular/core"
import {LoginEventModel} from "./../../Models/loginevent.model";

@Injectable()
export class LoginEventService
{
  loginevent:EventEmitter<LoginEventModel> =  new EventEmitter<LoginEventModel>();
  constructor()
  {

  }
  customEmit(eventmodel:LoginEventModel):void
  {
      this.loginevent.emit(eventmodel);
  }
}