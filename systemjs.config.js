(function (global) {
    System.config({
      map: {
        '@angular/core': '/node_modules/@angular/core/bundles/core.umd.js',
        '@angular/common': '/node_modules/@angular/common/bundles/common.umd.js',
        '@angular/compiler': '/node_modules/@angular/compiler/bundles/compiler.umd.js',
        '@angular/platform-browser': '/node_modules/@angular/platform-browser/bundles/platform-browser.umd.js',
        '@angular/platform-browser-dynamic': '/node_modules/@angular/platform-browser-dynamic/bundles/platform-browser-dynamic.umd.js',
        '@angular/http': '/node_modules/@angular/http/bundles/http.umd.js',
        '@angular/router': '/node_modules/@angular/router/bundles/router.umd.js',
        '@angular/forms': '/node_modules/@angular/forms/bundles/forms.umd.js',
        '@uirouter/core':'/node_modules/@uirouter/core/_bundles/ui-router-core.js',
        '@uirouter/angular':'/node_modules/@uirouter/angular/_bundles/ui-router-ng2.js',
        '@uirouter/rx':'/node_modules/@uirouter/rx/_bundles/ui-router-rx.js',
        'angular2-cookie':'node_modules/angular2-cookie',
        'rxjs': '/node_modules/rxjs'
      },
      packages: {
       'webapp/js': {
        defaultExtension: 'js',
        meta: {
          './*.js': {
            loader: 'systemjs-angular-loader.js'
          }
        }
      },
       rxjs: {
         defaultExtension: 'js'
       },
       'angular2-cookie':{defaultExtension:'js'}
     }
    });
  })(this);